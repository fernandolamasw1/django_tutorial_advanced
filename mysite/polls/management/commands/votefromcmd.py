from django.core.management.base import BaseCommand, CommandError
from polls.views import vote as votar

class Command(BaseCommand):
    help = 'Make a vote from command line'

    def handle(self, *args, **options):
        id_question = input("A que id de Question querés votar? ")
        id_choice = input("A que id de Choice querés votar? ")
        votar(id_question,id_choice)
        self.stdout.write(self.style.SUCCESS('Success!!'))