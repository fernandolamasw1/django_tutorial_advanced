from logging import exception
from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404
from django.shortcuts import get_object_or_404,render
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from setuptools import Command


from .models import Choice, Question


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'
    def get_queryset(self):
        return Question.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]



class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'
    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

class Resultado:
    @classmethod
    def exitoso(cls):
        return cls()

    def __init__(self):
        self._errores = []

    def agregar_error(self, mensaje_de_error):
        self._errores.append(mensaje_de_error)

    def es_exitoso(self):
        return not self.hay_errores()

    def errores(self):
        return self._errores

    def hay_errores(self):
        return len(self._errores) > 0

def obtener_model_a_partir_de_id(resultado, id, model_class):
    try:
        model = model_class.objects.get(pk=id)
    except Exception:
        resultado.agregar_error(f"Tu {model_class} id es invalido")
    return model

def obtener_choice_correspondiente_a_question(resultado, question, choice_id):
    try:
        selected_choice = question.choice_set.get(pk=choice_id)
    except Exception:
        resultado.agregar_error("tu choice no corresponde a la question")
    return selected_choice

def vote_http(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        choice_id = request.POST['choice']
        agregar_un_voto(question_id, choice_id)
    except(KeyError, Choice.DoesNotExist):
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "No seleccionaste una choice",
            })
    else:
        #selected_choice.votes += 1
        return HttpResponseRedirect(reverse('polls:results', args=(question_id,)))

def agregar_un_voto(question_id, choice_id):
    resultado = Resultado()
    # entre estos comentarios no deberia ser parte de vote porque vote es la accion directamente
    question = obtener_model_a_partir_de_id(resultado, question_id, Question)
    choice = obtener_model_a_partir_de_id(resultado, choice_id, Choice)
    if resultado.hay_errores():
        return resultado()
    # a partir de aca abajo es la accion de votar posta
    try:
        selected_choice = obtener_choice_correspondiente_a_question(resultado, question, choice_id)
        selected_choice.votes += 1
        selected_choice.save()
    except Exception:
        resultado.agregar_error("hubo un error al votar")
    return resultado

    
def vote_http_new(request, question_id):
    choice_id = request.POST['choice']
    resultado = agregar_un_voto(question_id, choice_id)
    return calcular_respuesta_http(request, resultado, question_id)

def vote_http_new_v2(request, question_id):
    choice_id = request.POST['choice']
    command = AgregarUnVoto(question_id, choice_id)
    resultado = command.execute()
    return calcular_respuesta_http(request, resultado, question_id)

class AgregarUnVoto():
    def __init__(self, question_id, choice_id):
        self._question_id = question_id
        self._choice_id = choice_id

    def execute(self):
        resultado = Resultado()
        # entre estos comentarios no deberia ser parte de vote porque vote es la accion directamente
        question = obtener_model_a_partir_de_id(resultado, self._question_id, Question)
        choice = obtener_model_a_partir_de_id(resultado, self._choice_id, Choice)
        if resultado.hay_errores():
            return resultado()
        # a partir de aca abajo es la accion de votar posta
        try:
            selected_choice = obtener_choice_correspondiente_a_question(resultado, question, self._choice_id)
            selected_choice.votes += 1
            selected_choice.save()
        except Exception:
            resultado.agregar_error("hubo un error al votar")
        return resultado


def calcular_respuesta_http(request, resultado, question_id):
    if resultado.es_exitoso():
        return HttpResponseRedirect(reverse('polls:results', args=(question_id,)))
    string_errores = errores_concatenados(resultado)
    return render(request, 'polls/detail.html',{
        'question': question_id,
        'error_message': string_errores,
    })

def errores_concatenados(resultado):
    texto_con_errores = ""
    for error in resultado.errores():
        texto_con_errores = texto_con_errores + " | " + error
    return texto_con_errores